const User = require('../models/User')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const crypto = require('crypto')

const hashConfig = require('../../config/auth')
const mailer = require('../../modules/mailer')

function generatorToken(params = {}) {
  return jwt.sign(params, hashConfig.secret, {
    expiresIn: 86400,
  })
}

module.exports = {
  register: async (req, res) => {
    const { email } = req.body
    try {

      if(await User.findOne({ email }))
        return res.status(400).send({ error: 'User already exists'})

      const user = await User.create(req.body)
      return res.send({
        user,
        token: generatorToken({ id: user.id })
      })

    } catch (e) {
      return res.status(400).send({ error: 'Registration failed'})
    }
  },

  authenticate: async (req, res) => {
    const { email, password } = req.body
    
    const user = await User.findOne({ email }).select('+password')

    if(!user)
      return res.status(400).send({ error: 'User not found'})

    if(! await bcrypt.compare(password, user.password))
      return res.status(400).send({ error: 'Invalid password'})

    user.password = undefined

    const token = generatorToken({ id: user.id })

    res.send({ user, token })
  },

  forgot_password: async (req, res) => {
    const { email } = req.body

    try {
      const user = await User.findOne({ email })

      if(!user)
        return res.status(400).send({ error: 'User not found' })
      
      const token = crypto.randomBytes(20).toString('hex')

      const now = new Date()
      now.setHours(now.getHours() + 1)

      await User.findByIdAndUpdate(user.id, {
        '$set': {
          passwordResetToken: token,
          passwordResetExpires: now
        }
      })

      mailer.sendMail({
        to: email,
        from: 'suport_tec@advir.com',
        template: 'auth/forgot_password',
        context: { token }
      }, err => {
        if(err)
          return res.status(400).send({ error: 'Cannot send forgot password email' })
        
        res.send()
      })

    } catch(err) {
      res.status(400).send({ error: 'Erro on forgot password, try again'})
    }
  },

  reset_password: async (req, res) => {
    const { email, password, token } = req.body

    try {

      const user = await User.findOne({ email })
        .select('+passwordResetToken passwordResetExpires')
      console.log(user)

      if(!user)
        return res.status(400).send({ error : 'User not found' })
      
      if(token !== user.passwordResetToken)
        return res.status(400).send({ error: 'Token invalid' })
      
      const now = new Date()
      if(now > user.passwordResetExpires) 
        return res.status(400).send({ error: 'Token expired' })

      user.password = password

      await user.save()

      return res.send()

    } catch (err) {
      res.status(400).send({ error: 'Cannot reset password, try again'})
    }

  }

}
