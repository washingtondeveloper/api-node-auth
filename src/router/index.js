const express = require('express')
const router = express.Router()

const authMiddleware = require('../app/middleware/auth')

const productsController = require('../app/controllers/productsController')
const authController = require('../app/controllers/authController')

/*User Auth*/
router.post('/auth/register', authController.register)
router.post('/auth/authenticate', authController.authenticate)
router.post('/auth/forgot_password', authController.forgot_password)
router.post('/auth/reset_passoword', authController.reset_password)

/*Products*/

/*Middlewares Products*/
router.use('/products', authMiddleware)
/*Products default*/
router.get('/products', productsController.getProducts)

module.exports = router
